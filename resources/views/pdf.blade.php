<div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">File Preview</h5>
    </div>
    <div class="modal-body">
        @if($link !== 0)
		<div style="min-height:650px;">
			<iframe title="Advertisement" id="test.pdf" src="{{ $link }}#toolbar=0" frameborder="0" marginwidth="0" marginheight="0" scrolling="yes" width="100%" style="position: relative; height: 650px;"> </iframe>
		</div>
		@else
		<p style="text-align: center;font-weight: bold;">
			File Tidak Ditemukan
		</p>
		@endif
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
    </div>
<div class="loading dimmer padder-v">
    <div class="loader"></div>
</div>
