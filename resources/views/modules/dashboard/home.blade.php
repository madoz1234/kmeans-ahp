@extends('layouts.base')

@push('css')
    <link rel="stylesheet" href="#" type="text/css" />
    <link rel="stylesheet" href="{{ asset('libs/assets/one-calender/simple-calendar.css') }}" type="text/css" />
@endpush

@push('js')
    <script src="{{ asset('libs/assets/one-calender/jquery.simple-calendar.js') }}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://apexcharts.com/samples/assets/stock-prices.js') }}"></script> --}}
@endpush

@section('title', 'Dashboard')

@section('side-header')
<div style="margin-right: 14px;">
	<span><i class="glyphicon glyphicon-home"></i></span>
</div>
@endsection

@push('styles')
<style>
	.nav-pills > li.active > a,
	.nav-pills > li.active > a:hover,
	.nav-pills > li.active > a:focus {
	  color: #fff;
	  background-color: #448BFF;
	}
	.nav-pills > li > a {
	  border-radius: 0px;
	}
</style>
@endpush

@push('scripts')
<script type="text/javascript">
	var options1 = {
          series: [{
            name: "Total Penjualan",
            data: [10, 41, 35, 51, 49, 62, 69, 91, 148, 91, 148, 148]
        }],
          chart: {
          height: 350,
          type: 'line',
          zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'straight'
        },
        title: {
          text: 'Total Penjualan Perbulan',
          align: 'left'
        },
        grid: {
          row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
            opacity: 0.5
          },
        },
        xaxis: {
          categories: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        }
        };

        var chart1 = new ApexCharts(document.querySelector("#total_penjualan"), options1);
        chart1.render();


        var options2 = {
          series: [{
          name: 'Total Penjualan Harian',
          data: [4, 5, 7, 6, 1, 8, 3, 6, 6, 5, 3, 6, 6, 5, 3, 0, 6, 8, 3, 6, 6, 5, 3, 0]
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00', '24:00'],
        },
        yaxis: {
          title: {
            text: 'Penjualan'
          }
        },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val + " Penjualan"
            }
          }
        },
        colors: ['#9C27B0'],
        };

        var chart2 = new ApexCharts(document.querySelector("#harian"), options2);
        chart2.render();

        var options3 = {
          series: [{
            name: "Pendapatan",
            data: [100000, 150000, 14000, 200000, 250000, 122500, 143000, 345000, 542300, 143000, 200000, 150000]
          },
          {
            name: "Pengeluaran",
            data: [1500, 40000, 62, 42, 13, 18, 29, 37, 36, 51, 32, 35]
          },
        ],
          chart: {
          height: 350,
          type: 'line',
          zoom: {
            enabled: false
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          width: [5, 7, 5],
          curve: 'straight',
          dashArray: [0, 8, 5]
        },
        legend: {
          tooltipHoverFormatter: function(val, opts) {
            return val + '  Rp.' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + '.-'
          }
        },
        markers: {
          size: 0,
          hover: {
            sizeOffset: 6
          }
        },
        xaxis: {
          categories: ['01 Jan', '02 Jan', '03 Jan', '04 Jan', '05 Jan', '06 Jan', '07 Jan', '08 Jan', '09 Jan',
            '10 Jan', '11 Jan', '12 Jan'
          ],
        },
        tooltip: {
          y: [
            {
              title: {
                formatter: function (val) {
                  return val
                }
              }
            },
            {
              title: {
                formatter: function (val) {
                  return val
                }
              }
            }
          ]
        },
        grid: {
          borderColor: '#f1f1f1',
        }
        };

        var chart3 = new ApexCharts(document.querySelector("#pendapatan_harian"), options3);
        chart3.render();

        var options4 = {
          series: [{
            name: "Pendapatan",
            data: [100000, 150000, 14000, 200000, 250000, 122500, 143000, 345000, 542300, 143000, 200000, 150000]
          },
          {
            name: "Pengeluaran",
            data: [1500, 40000, 62, 42, 13, 18, 29, 37, 36, 51, 32, 35]
          },
        ],
          chart: {
          height: 350,
          type: 'line',
          zoom: {
            enabled: false
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          width: [5, 7, 5],
          curve: 'straight',
          dashArray: [0, 8, 5]
        },
        legend: {
          tooltipHoverFormatter: function(val, opts) {
            return val + '  Rp.' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + '.-'
          }
        },
        markers: {
          size: 0,
          hover: {
            sizeOffset: 6
          }
        },
        xaxis: {
          categories: ['01 Jan', '02 Jan', '03 Jan', '04 Jan', '05 Jan', '06 Jan', '07 Jan', '08 Jan', '09 Jan',
            '10 Jan', '11 Jan', '12 Jan'
          ],
        },
        tooltip: {
          y: [
            {
              title: {
                formatter: function (val) {
                  return val
                }
              }
            },
            {
              title: {
                formatter: function (val) {
                  return val
                }
              }
            }
          ]
        },
        grid: {
          borderColor: '#f1f1f1',
        }
        };

        var chart4 = new ApexCharts(document.querySelector("#pendapatan_bulanan"), options4);
        chart4.render();
</script>
@endpush

@section('body')
		<div class="row" style="margin: 0px 0px 20px 0px">
			<div class="col-sm-12 text-right">
				<form id="dataFilters" class="form-inline" role="form">
					<div class="form-group">
						<label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
						<input type="text" class="form-control tahuns" name="tahuns" id="tahuns" data-toggle="years" placeholder="Pilih Tahun" value="{{ \Carbon\Carbon::now()->format('Y') }}">
					</div>
				</form>
			</div>
		</div>

		<div class="row row-sm">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="panel b-a">
					<div class="panel-heading text-left no-border">
						<span class="h4 m-b font-thin">Total Penjualan Perbulan</span>          
					</div>
					<div class="wrapper b-b b-light">
						<div id="total_penjualan"></div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="panel b-a">
					<div class="panel-heading text-left no-border">
						<span class="h4 m-b font-thin">Total Penjualan Harian</span>          
					</div>
					<div class="wrapper b-b b-light">
						<div id="harian"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row row-sm">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="panel b-a">
					<div class="panel-heading text-left no-border">
						<span class="h4 m-b font-thin">Total Pendapatan Vs Pengeluaran Harian</span>          
					</div>
					<div class="wrapper b-b b-light">
						<div id="pendapatan_harian"></div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="panel b-a">
					<div class="panel-heading text-left no-border">
						<span class="h4 m-b font-thin">Total Pendapatan Vs Pengeluaran Bulanan</span>          
					</div>
					<div class="wrapper b-b b-light">
						<div id="pendapatan_bulanan"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection