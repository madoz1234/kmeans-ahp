@extends('layouts.list')

@section('title', 'Kriteria')

@section('side-header')
   <nav aria-label="breadcrumb">
	  <ol class="breadcrumb" style="background-color: transparent !important;">
	  	<?php $i=1; $last=count($breadcrumb);?>
	  	 @foreach ($breadcrumb as $name => $link)
	  	 	@if($i++ != $last)
		  	 	<li class="breadcrumb-item"><a href="{{ $link }}" style="color:blue;">{{ $name }}</a></li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{ $name }}</li>
            @endif
	  	 @endforeach
	  </ol>
	</nav>
@endsection

@section('filters')
    <div class="form-group">
        <label class="control-label m-r-sm" for="filters"><i class="fa fa-filter"></i> &nbsp; Filter</label>
        <label class="control-label sr-only" for="filter-kode">Kode</label>
        <input type="text" class="form-control filter-control" name="filter[kode]" data-post="kode" placeholder="Kode">
        <label class="control-label sr-only" for="filter-name">Nama Kriteria</label>
        <input type="text" class="form-control filter-control" name="filter[name]" data-post="name" placeholder="Nama Kriteria">
    </div>
@endsection