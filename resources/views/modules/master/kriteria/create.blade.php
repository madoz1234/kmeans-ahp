<form action="{{ route($routes.'.store') }}" method="POST" id="formData">
    @csrf

    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Tambah Data Kriteria</h5>
    </div>
    <div class="modal-body">
    	<div class="form-group field">
            <label class="control-label">Kode</label>
            <input type="text" name="kode" class="form-control" placeholder="Kode" required="">
        </div>
        <div class="form-group field">
            <label class="control-label">Nama Kriteria</label>
            <input type="text" name="nama" class="form-control" placeholder="Nama Kriteria" required="">
        </div>
        <div class="form-group field">
            <label class="control-label">Bobot Kriteria</label>
            <input type="number" name="bobot" class="form-control" placeholder="Bobot Kriteria" required="">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>