@extends('layouts.base')

@push('css')
<link rel="stylesheet" href="#" type="text/css" />
<link rel="stylesheet" href="{{ asset('libs/assets/one-calender/simple-calendar.css') }}" type="text/css" />
@endpush

@push('js')
<script src="{{ asset('libs/assets/one-calender/jquery.simple-calendar.js') }}"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script src="https://apexcharts.com/samples/assets/stock-prices.js') }}"></script> --}}
@endpush

@section('title', 'Hasil Kmeans')

@section('side-header')
<div style="margin-right: 14px;">
  <span><i class="glyphicon glyphicon-home"></i></span>
</div>
@endsection

@push('styles')
@endpush

@push('scripts')
@endpush

@section('body')
<div class="row row-sm">
  <div class="panel b-a">
    <div class="panel-heading text-left no-border">
      <span class="h4 m-b font-thin">Nilai Alternatif</span>          
    </div>
    <div class="wrapper b-b b-light">
      <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
        <tbody>
          <tr>
            <td style="text-align: center;font-weight: bold;" rowspan="2">Alternatif</td>
            <td style="text-align: center;font-weight: bold;" colspan={{count($bantuan)}}>Bantuan</td>
          </tr>
          <tr>
            @foreach($bantuan as $bantuans)
            <td style="">{{$bantuans->nama}}</td>
            @endforeach
          </tr>
          @foreach($alternatif as $data)
          <tr>
            <td style="">{{ $data->nama }}</td>
            @php
            $n = $nilai->where('alternatif_id', $data->id)->first();
            @endphp
            @foreach($n->detail as $details)
            <td style="">{{$details->nilai}}</td>
            @endforeach
          </tr>
          @endforeach
        </tbody>
      </table>
      <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
        <tbody>
          <tr>
            <td style="text-align: center;font-weight: bold;" rowspan="2">Cluster</td>
            <td style="text-align: center;font-weight: bold;" colspan={{count($bantuan)}}>Bantuan</td>
          </tr>
          <tr>
            @foreach($bantuan as $bantuans)
            <td style="">{{$bantuans->nama}}</td>
            @endforeach
          </tr>
          @foreach($cluster as $data)
          <tr>
            <td style="">{{ $data->nama }}</td>
            @foreach($bantuan as $ban)
            @foreach($data->detail->where('bantuanid', $ban->id) as $details)
                <td style="">{{$details->nilai}}</td>
            @endforeach
            @endforeach
          </tr>
          @endforeach
        </tbody>
      </table>
      <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
          <tbody>
            <tr>
              <td style="width: 20px;" rowspan="2">No</td>
              <td style="width: 200px;" rowspan="2">Nama Alternatif</td>
              <td style="text-align: center;" colspan="{{count($bantuan)}}">Nilai Bantuan</td>
            </tr>
            <tr>
              @foreach($bantuan as $bantuans)
              <td style="">{{ $bantuans->nama }}</td>
              @endforeach
            </tr>
            @php
              $i=1;
            @endphp
            @foreach($ahp as $ahps)
              <tr>
                <td style="">{{ $i }}</td>
                <td style="">{{ $ahps->alternatif->nama}}</td>
                @foreach($nilai->where('alternatif_id', $ahps->alternatif->id) as $alternatifs)
                  @foreach($bantuan as $bantuans)
                  @php 
                    $nilai_detail = $alternatifs->detail->where('bantuan_id', $bantuans->id)->first();
                  @endphp
                  <td style="">{{ $nilai_detail->nilai }}</td>
                  @endforeach
                @endforeach
              </tr>
               @php
                $i++;
              @endphp
            @endforeach
          </tbody>
      </table>
      <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
          <tbody>
            <tr>
              <td style="width: 20px;" rowspan="2">No</td>
              <td style="width: 200px;" rowspan="2">Nama Alternatif</td>
              <td style="text-align: center;" colspan="{{count($bantuan)}}">Jarak Ke Cluster</td>
            </tr>
            <tr>
              @foreach($cluster as $clusters)
              <td style="">{{ $clusters->nama }}</td>
              @endforeach
            </tr>
            @php
              $i=1;
              $hasil =0;
            @endphp
            @foreach($ahp as $ahps)
              <tr>
                <td style="">{{ $i }}</td>
                <td style="">{{ $ahps->alternatif->nama}}</td>
                @foreach($nilai->where('alternatif_id', $ahps->alternatif->id) as $alternatifs)
                  @foreach($cluster as $clusters)
                    @foreach($clusters->detail as $details)
                      @php 
                        $nilai_detail = $alternatifs->detail->where('bantuan_id', $details->bantuanid)->first()->nilai;
                        $jum = pow($nilai_detail - $details->nilai, 2);
                        $hasil +=$jum;
                      @endphp
                    @endforeach
                    @php
                      $kmeans = App\Models\Kmeans\Kmeans::where('alternatifid', $ahps->alternatif->id)->first();
                      if(is_null($kmeans)){
                          $new = new App\Models\Kmeans\Kmeans;
                          $new->alternatifid = $ahps->alternatif->id;
                          $new->save();
                          $cekz = App\Models\Kmeans\KmeansDetail::where('kmeansid', $kmeans->id)->where('cluster_detail_id', $details->id)->first();
                          if(is_null($cekz)){
                            $newdetail = new App\Models\Kmeans\KmeansDetail;
                            $newdetail->kmeansid = $kmeans->id;
                            $newdetail->cluster_detail_id = $details->id;
                            $newdetail->nilai = round($hasil);
                            $newdetail->save();
                          }else{
                            $newdetail = App\Models\Kmeans\KmeansDetail::find($cekz->id);
                            $newdetail->kmeansid = $kmeans->id;
                            $newdetail->cluster_detail_id = $details->id;
                            $newdetail->nilai = round($hasil);
                            $newdetail->save();
                          }
                      }else{
                          $new = App\Models\Kmeans\Kmeans::find($kmeans->id);
                          $new->alternatifid = $ahps->alternatif->id;
                          $new->save();

                          $cekz = App\Models\Kmeans\KmeansDetail::where('kmeansid', $kmeans->id)->where('cluster_detail_id', $details->id)->first();

                          if(is_null($cekz)){
                            $newdetail = new App\Models\Kmeans\KmeansDetail;
                            $newdetail->kmeansid = $kmeans->id;
                            $newdetail->cluster_detail_id = $details->id;
                            $newdetail->nilai = round($hasil);
                            $newdetail->save();
                          }else{
                            $newdetail = App\Models\Kmeans\KmeansDetail::find($cekz->id);
                            $newdetail->kmeansid = $kmeans->id;
                            $newdetail->cluster_detail_id = $details->id;
                            $newdetail->nilai = round($hasil);
                            $newdetail->save();
                          }
                      }
                    @endphp
                    <td style="">{{ $hasil }}</td>
                  @endforeach
                @endforeach
              </tr>
               @php
                $i++;
              @endphp
            @endforeach
          </tbody>
      </table>

      <table id="example" class="table table-bordered m-t-none" style="width: 100%;">
          <tbody>
            <tr>
              <td style="width: 20px;" rowspan="2">No</td>
              <td style="width: 200px;" rowspan="2">Nama Alternatif</td>
              <td style="text-align: center;" colspan="{{count($bantuan)}}">Jarak Ke Cluster</td>
            </tr>
            <tr>
              @foreach($cluster as $clusters)
              <td style="">{{ $clusters->nama }}</td>
              @endforeach
            </tr>
            @php
              $i=1;
              $hasil =0;
            @endphp
            @foreach($ahp as $ahps)
              <tr>
                <td style="">{{ $i }}</td>
                <td style="">{{ $ahps->alternatif->nama}}</td>
                @foreach($nilai->where('alternatif_id', $ahps->alternatif->id) as $alternatifs)
                  @foreach($cluster as $clusters)
                    <td style="">a</td>
                  @endforeach
                @endforeach
              </tr>
               @php
                $i++;
              @endphp
            @endforeach
          </tbody>
      </table>
    </div>
  </div>
</div>
@endsection