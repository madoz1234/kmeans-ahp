<form action="{{ route($routes.'.update', $record->id) }}" method="POST" id="formData">
    @method('PATCH')
    @csrf
    <input type="hidden" name="id" value="{{ $record->id }}">
    <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Ubah Data User</h5>
    </div>
    <div class="modal-body">
        {{-- <p class="text-muted">Please fill the information to continue</p> --}}
        <div class="form-group">
            <label class="control-label">NIP</label>
            <input type="text" name="nip" class="form-control" placeholder="NIP" value="{{ $record->nip }}" required>
        </div>
        <div class="form-group">
            <label class="control-label">Nama</label>
            <input type="text" name="name" class="form-control" placeholder="Nama" value="{{ $record->name }}" required>
        </div>
        <div class="clearfix">
            <div class="col-sm-6">
                <div class="form-group" style="margin-right: 13px;">
                    <label class="control-label">Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Email" value="{{ $record->email }}" required>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">No. Telepon</label>
                    <input type="text" name="phone" class="form-control" placeholder="No. Telepon" value="{{ $record->phone }}"  required="">
                </div>
            </div>
        </div>
        <div class="clearfix">
            <div class="col-sm-6">
                <div class="form-group" style="margin-right: 13px;">
                    <label class="control-label">Password <em class="text-muted">(isi jika akan ubah password)</em></label>
                    <input type="password" name="password" class="form-control" placeholder="Password" required>   
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label">Konfirmasi Password</label>
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Konfirmasi Password" required>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label">Tipe</label>
            <select class="selectpicker form-control tipe" name="tipe" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                <option value="1" @if($record->tipe == 1) selected @endif>Pegawai</option>
                <option value="2" @if($record->tipe == 2) selected @endif>Non-Pegawai</option>
            </select>                  
        </div>
        <div class="form-group">
            <label class="control-label">Kategori</label>
            <select class="selectpicker form-control kategori" data-size="2" name="kategori" id="kategori" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                <option value="1" @if($record->kategori == 1) selected @endif>Business Unit</option>
                <option value="2" @if($record->kategori == 2) selected @endif>Corporate Office</option>
            </select>                  
        </div>
        <div class="form-group isi-bu" style="display: none;">
            <label class="control-label">Business Unit (BU)</label>
            <select class="selectpicker form-control" name="bu_id" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                @foreach(App\Models\Master\BU::get() as $bu)
                    <option value="{{ $bu->id }}" @if($record->kategori_id == $bu->id) selected @endif>{{ $bu->nama }}</option>
                @endforeach
            </select>                  
        </div>
        <div class="form-group isi-co" style="display: none;">
            <label class="control-label">Corporate Office (CO)</label>
            <select class="selectpicker form-control" name="co_id" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
                @foreach(App\Models\Master\CO::get() as $co)
                    <option value="{{ $co->id }}" @if($record->kategori_id == $co->id) selected @endif>{{ $co->nama }}</option>
                @endforeach
            </select>                  
        </div>
        <div class="form-group hiddens" id="non-pegawai" style="display: none;">
            <label class="control-label">Role</label>
            <select class="selectpicker form-control cari" 
                    name="role" 
                    data-style="btn-default" 
                    data-live-search="true" 
                    title="(Pilih Role)">
                @foreach(App\Models\Auths\Role::whereIn('name', ['bpk', 'svp', 'vendor'])->get() as $role)
                    <option value="{{ $role->id }}" @if($record->hasRole($role->name)) selected @endif>{{ $role->name }}</option>
                @endforeach
            </select>                  
        </div>
        <div class="form-group hiddens" id="pegawai">
            <label class="control-label">Role</label>
            <select class="selectpicker form-control cari" 
                    name="roles" 
                    data-style="btn-default" 
                    data-live-search="true" 
                    title="(Pilih Role)">
                @foreach(App\Models\Auths\Role::all() as $role)
                    <option value="{{ $role->id }}" @if($record->hasRole($role->name)) selected @endif>{{ $role->name }}</option>
                @endforeach
            </select>                  
        </div>
        <div class="form-group isi" style="display: none;">
            <label class="control-label">Fungsi</label>
            <select class="pilihan selectpicker col-sm-12 show-tick" data-size="3" name="fungsi" data-style="btn-default" data-live-search="true" title="(Pilih Salah Satu)">
				<option value="1" @if($record->fungsi == 1) selected @endif>Operasional</option>
				<option value="2" @if($record->fungsi == 2) selected @endif>Keuangan</option>
				<option value="3" @if($record->fungsi == 3) selected @endif>Sistem</option>
			</select>                  
        </div>
        <div class="form-group isi" style="display: none;">
            <label class="control-label">Inisial</label>
            <input type="text" name="inisial" class="form-control" placeholder="Inisial" required value="{{ $record->inisial }}">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Kembali</button>
        <button type="button" class="btn btn-simpan save button">Simpan</button>
    </div>

    <div class="loading dimmer padder-v">
        <div class="loader"></div>
    </div>
</form>