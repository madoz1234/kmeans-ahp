@extends('layouts.auth')

@section('content')
<form class="form-signin">
    <div class="d-md-none d-sm-block text-center" style="padding-bottom: 40px">
        <img width="30%" src="{{  asset('src/img/logo-mini.png') }}">
        <h2>
            PT. Waskita Karya (Persero) Tbk
        </h2>
    </div>
    @csrf
    <div class="mb-5">
        <strong>Maaf</strong> <br>Anda telah keluar aplikasi silahkan login ulang.
    </div>
    <div class="row">
        <div class="col-lg-12">
            <a href="{{ url('login') }}" class="btn btn-raised btn-wave float-left mb-4 w-xs btn-login-check text-white">Login Web</a>
            <a href="{{ url('auth') }}" class="btn btn-raised btn-wave float-right btn-login-check mb-4 w-xs indigo text-white">Login IDPROO</a>
        </div>
    </div>

    {{-- <div class="footer py-3 w-50 text-left">
        @if (Route::has('password.request'))
            <a class="text-muted" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        @endif
    </div> --}}
</form>
@endsection
