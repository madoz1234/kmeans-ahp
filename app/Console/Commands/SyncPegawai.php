<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\Models\Dms;
use App\Models\Files;
use App\Models\Auths\User;
use App\Models\Master\Lokasi;

use Illuminate\Filesystem\Filesystem;
use Storage;
use Carbon\Carbon;
use Hash;
use App\Models\Master\BU;
use App\Models\Master\CO;
use App\Models\Temp\Division;

class SyncPegawai extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:pegawai';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Pegawai';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $countUpdate = 0;
      $count = 0;
      $countNew = 0;
      $countFailed = 0;
      $countBU = 0;
      $countCO = 0;

      $client = new Client;
      $response = $client->request('GET', 'https://west.waskita.co.id/page/tlcc/apiwest/apiwest.php?secret='.$this->login().'&group=profile');

      $users = json_decode($response->getBody()->getContents());

      if(count($users) > 0)
      {
          foreach($users as $user)
          {
              $count++;
              try {
                $check = User::where('nip', $user->emp_id)->first();

                if(!$check)
                {
                  $check = User::where('email', $user->emp_email)->first();
                  if(!$check)
                  {
                    $check = new User;
                    $check->password = Hash::make('password');
                    $countNew++;
                  }else{
                    $countUpdate++;
                  }
                }else{
                  $countUpdate++;
                }
                $check->name = $user->emp_name;
                $check->email = $user->emp_email;
                $check->phone = $user->emp_hp;
                $check->posisi = $user->posisi;
                $check->kategori = $this->checkDivision($user->persarea_name);

                if($this->checkDivision($user->persarea_name) == 2)
                {
                    $countCO++;
                }

                if($this->checkDivision($user->persarea_name) == 1)
                {
                    $countBU++;
                }

                $check->nama_divisi = $user->persarea_name;
                $check->kode_divisi = $user->persarea_code;
                $check->no_ktp = $user->emp_reg_id;
                $check->nip = $user->emp_id;
                $check->emp_og_unit = $user->emp_og_unit;
                $check->kategori_id = 0;
                
                $emp_og_unit = $this->getDivision($user->emp_og_unit);

                $bu = BU::where('obj_id', $emp_og_unit)->first();
                if($bu)
                {
                    $check->kategori_id = $bu->id;
                }

                $co = CO::where('obj_id', $emp_og_unit)->first();
                if($co)
                {
                    $check->kategori_id = $co->id;
                }

                $check->save();

                $this->info($count.'/'.count($users));

              }catch (\Exception $exception){
                $countFailed++;
                $this->info('Gagal '.$exception->getMessage());
                $this->info($count.'/'.count($users));
              }

          }

          $this->info('USER BARU : '.$countNew);
          $this->info('USER UPDATE : '.$countUpdate);
          $this->info('USER GAGAL : '.$countFailed);
          $this->info('USER BU : '.$countBU);
          $this->info('USER CO : '.$countCO);

      }
    }

    public function login()
    {
      $client = new Client;

      $response = $client->request('GET', 'https://west.waskita.co.id/page/tlcc/apiwest/login_mobile.php?username=app-welcome&password=hris&fcm_token');

      return json_decode($response->getBody()->getContents())->secret;

    }

    public function checkDivision($name)
    {
        switch($name)
        {
            case 'Kantor Pusat' : return 2;
            break;
            case 'Building Division': return 1;
            break;
            case 'Infra I Division': return 1;
            break;
            case 'Infra II Division': return 1;
            break;
            case 'Infra III Division': return 1;
            break;
            case 'EPC Division': return 1;
            break;

            default: return 0;
        }

    }

    public function getDivision($og_unit)
    {
        $check = Division::where('obj_id', $og_unit)->first();
        if($check)
        {
            if($check->obj_level == 'DIRECTORATE')
            {
                return 0;
            }else if($check->obj_level == 'DIVISION')
            {
                return $check->obj_id;
            }else if($check->obj_level == 'DEPARTMENT')
            {
                $div = Division::where('obj_id', $check->parent_id)->first();
                if($div)
                {
                    return $div->obj_id;
                }
                return 0;
            }else if($check->obj_level == 'PROJECT')
            {
                return $this->getProjectDivision($check->parent_id);
            }
        }
    }

    public function getProjectDivision($parent_id)
    {
        $return = 0;

        $check = Division::where('obj_id', $parent_id)->first();
        if($check)
        {
            if($check->obj_level != 'DIVISION')
            {
                $return = $this->getProjectDivision($check->parent_id);
            }else{
                $return = $check->obj_id;
            }
        }

        return $return;
    }

}
