<?php

namespace App\Models;

use App\Models\Model;
use App\Models\Auths\User;
use Mail;
use App\Mail\NotificationEmail;

class Notification extends Model
{
    protected $table 		= 'trans_notification';
    protected $fillable 	= ['user_id','parent_id','url','modul','status','stage','keterangan'];

    /* data ke log */
    protected $log_table    = 'log_trans_notification';
    protected $log_table_fk = 'ref_id';

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function saveByFill($data)
    {
        $this->fill($data);
        $this->save();
    }

    public function sendEmail()
    {
        Mail::to($this->user->email)->queue(new NotificationEmail($this, $this->user, 'Notification | IA Online WASKITA'));
    }
}
