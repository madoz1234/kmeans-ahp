<?php

namespace App\Models\Auths;

use App\Models\Tests\Participation;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\Traits\Utilities;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles, Utilities;

    /* JWT */
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    /* End of JWT */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['last_login'];
    protected $fillable = [
        'name', 'email', 'password', 'last_login', 'alamat', 'status', 'nip','jk','tgl_lahir',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $table = 'sys_users';

    public function tests()
    {
        return $this->hasMany(Participation::class, 'user_id');
    }

    public function testStats()
    {
        return $this->tests()->passed()->count().' / '.$this->tests->count();
    }

    public function canTake($test_id)
    {
        $taken = $this->tests()->whereHas('test', function ($query) use ($test_id) {
            return $query->where('id', $test_id)
                         ->where('trial', 0);
        })->first();

        return is_null($taken);
    }

    public function takeTest($test)
    {
        DB::beginTransaction();

        $test = $this->tests()->create([
            'test_id' => $test->id,
            'start'   => Carbon::now(),
            'status'  => 'ongoing'
        ]);

        if (!$test->generateQuestions()) {
            DB::rollback();
        }

        DB::commit();

        return $test;
    }

    public function updateActivity()
    {
        $this->last_activity = Carbon::now()->format('Y-m-d H:i:s');
        $this->save();
    }

    public function flushActivity()
    {
        $this->last_activity = NULL;
        $this->save();
    }

    public function setLoginWeb()
    {
        $this->login_web = 1;
        $this->save();
    }

    public function updateLoginWeb()
    {
        $this->login_web = 0;
        $this->save();
    }

    public function checkToken()
    {
        if($this->login_web == 0)
        {
            if($this->access_token != NULL)
            {
                // [TODO] Certificate
            }else{
                $this->flushActivity();
                Auth::logout();
            }
        }
    }
}
