<?php

namespace App\Models\Ahp;

use App\Models\Model;
use App\Models\Master\Kriteria;

class PriorityKriteria extends Model
{
    /* default */
    protected $table 		= 'trans_ev_kriteria';
    protected $fillable 	= ['kriteriaid1','kriteriaid2','nilai'];

    /* data ke log */
    protected $log_table    = 'log_trans_ev_kriteria';
    protected $log_table_fk = 'ref_id';
    /* relation */
    
    public function kriteria1(){
        return $this->belongsTo(Kriteria::class, 'kriteriaid1' , 'id');
    }

    public function kriteria2(){
        return $this->belongsTo(Kriteria::class, 'kriteriaid2' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
