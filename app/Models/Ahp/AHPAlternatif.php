<?php

namespace App\Models\Ahp;

use App\Models\Model;
use App\Models\Master\Alternatif;
use App\Models\Master\Kriteria;

class AHPAlternatif extends Model
{
    /* default */
    protected $table 		= 'trans_rasio_alternatif_ahp';
    protected $fillable 	= ['alternatif_1_id','alternatif_2_id','kriteria_id','ratio'];

    /* data ke log */
    protected $log_table    = 'log_trans_rasio_alternatif_ahp';
    protected $log_table_fk = 'ref_id';
    /* relation */
    
    public function alternatif1(){
        return $this->belongsTo(Alternatif::class, 'alternatif_1_id' , 'id');
    }

    public function alternatif2(){
        return $this->belongsTo(Alternatif::class, 'alternatif_2_id' , 'id');
    }

    public function kriteria(){
        return $this->belongsTo(Kriteria::class, 'kriteria_id' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
