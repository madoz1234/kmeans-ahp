<?php

namespace App\Models\Ahp;

use App\Models\Model;
use App\Models\Master\Kriteria;

class AHPKriteria extends Model
{
    /* default */
    protected $table 		= 'trans_rasio_kriteria_ahp';
    protected $fillable 	= ['kriteria_1_id','kriteria_2_id','ratio'];

    /* data ke log */
    protected $log_table    = 'log_trans_rasio_kriteria_ahp';
    protected $log_table_fk = 'ref_id';
    /* relation */
    
    public function kriteria1(){
        return $this->belongsTo(Kriteria::class, 'kriteria_1_id' , 'id');
    }

    public function kriteria2(){
        return $this->belongsTo(Kriteria::class, 'kriteria_2_id' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
