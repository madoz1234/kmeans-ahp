<?php

namespace App\Models\Master;

use App\Models\Model;
use App\Models\Master\Bantuan;
use App\Models\Master\Cluster;

class ClusterDetail extends Model
{
    /* default */
    protected $table 		= 'ref_cluster_detail';
    protected $fillable 	= ['clusterid','bantuanid','nilai'];

    /* data ke log */
    // protected $log_table    = 'log_ref_bantuan';
    // protected $log_table_fk = 'ref_id';
    /* relation */
    public function bantuan(){
        return $this->belongsTo(Bantuan::class, 'bantuanid' , 'id');
    }

    public function cluster(){
        return $this->belongsTo(Cluster::class, 'clusterid' , 'id');
    }
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
