<?php

namespace App\Models\Master;

use App\Models\Model;

class Kriteria extends Model
{
    /* default */
    protected $table 		= 'ref_kriteria';
    protected $fillable 	= ['kode','nama','bobot'];

    /* data ke log */
    protected $log_table    = 'log_ref_kriteria';
    protected $log_table_fk = 'ref_id';
    /* relation */
    /* mutator */
    // insert code here
    /* scope */
    // insert code here


    /* custom function */
    // insert code here    
}
