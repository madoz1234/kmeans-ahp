<?php

namespace App\Http\Requests\Master;

use App\Http\Requests\FormRequest;

class BantuanRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'nama'            			=> 'required|max:200|unique:ref_bantuan,nama,'.$this->get('id'),
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'nama.required'            		=> 'Nama Bantuan tidak boleh kosong',
        	'nama.unique'            		=> 'Nama Bantuan sudah ada',
       ];
    }
}
