<?php

namespace App\Http\Requests\AHPAlternatif;

use App\Http\Requests\FormRequest;

class AHPAlternatifRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.data.*.ratio'            			=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'detail.*.data.*.ratio.required'            		=> 'Nilai tidak boleh kosong',
       ];
    }
}
