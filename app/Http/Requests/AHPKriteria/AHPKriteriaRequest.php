<?php

namespace App\Http\Requests\AHPKriteria;

use App\Http\Requests\FormRequest;

class AHPKriteriaRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
    	$input = $this->all();
    	$return = [
            'detail.*.ratio'            			=> 'required',
        ];
		return $return;
    }

    public function messages()
    {
    	return [
        	'detail.*.ratio.required'            		=> 'Nilai tidak boleh kosong',
       ];
    }
}
