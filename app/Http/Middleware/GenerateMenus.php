<?php
namespace App\Http\Middleware;

use Closure;
use Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('sideMenu', function ($menu) {

	        $menu->add('AHP', 'ahp')
                  ->data('icon', 'fa fa-money');
	             $menu->aHP->add('Ratio Kriteria', 'ahp/kriteria')
                 	 ->data('perms', 'ahp-kriteria')
	            	 ->active('ahp/kriteria/*');
	           	 $menu->aHP->add('Ratio Alternatif', 'ahp/alternatif')
                     ->data('perms', 'ahp-alternatif')
                     ->active('ahp/alternatif/*');
            $menu->add('KMEANS', 'kmeans')
                  ->data('icon', 'fa fa-money');
	             $menu->kMEANS->add('Nilai Alternatif', 'kmeans/nilai')
                 	 ->data('perms', 'kmeans-nilai')
	            	 ->active('kmeans/nilai/*');
	           	 $menu->kMEANS->add('Hasil', 'kmeans/hasil')
                 	 ->data('perms', 'kmeans-hasil')
	            	 ->active('kmeans/hasil/*');
            $menu->add('Data Master', 'master')
                  ->data('icon', 'fa fa-book');
	             $menu->dataMaster->add('Kriteria', 'master/kriteria')
                 	 ->data('perms', 'master-kriteria')
	            	 ->active('master/kriteria/*');
                 $menu->dataMaster->add('Bantuan', 'master/bantuan')
                     ->data('perms', 'master-bantuan')
                     ->active('master/bantuan/*');
                 $menu->dataMaster->add('Cluster', 'master/cluster')
                     ->data('perms', 'master-cluster')
                     ->active('master/cluster/*');
	             $menu->dataMaster->add('Alternatif', 'master/alternatif')
                 	 ->data('perms', 'master-alternatif')
	            	 ->active('master/alternatif/*');
        });
        return $next($request);
    }
}
