<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use App\Models\Auths\User;
use Carbon\Carbon;
use Auth;
use Illuminate\Validation\ValidationException;
use Jumbojett\OpenIDConnectClient;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/topsis/nilai';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        auth()->user()->flushActivity();
        $this->performLogout($request);
        return redirect('/login');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    protected function attemptLogin(Request $request)
    {

        if (\Auth::validate($request->only(['email', 'password']))) {
           $user = User::whereEmail($request->email)->first();
           if($user->last_activity != NULL && !Carbon::parse($user->last_activity)->addHours(2)->isPast())
           {
               throw ValidationException::withMessages([
                   $this->username() => [trans('auth.login')],
               ]);
           }
        }

       return $this->guard()->attempt(
           $this->credentials($request), $request->filled('remember')
       );
    }
  /**
   * The user has been authenticated.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  mixed  $user
   * @return mixed
   */
  protected function authenticated(Request $request, $user)
  {
      
  }
}
