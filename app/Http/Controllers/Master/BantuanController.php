<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\BantuanRequest;
use App\Models\Master\Bantuan;
use App\Models\SessionLog;

use DB;

class BantuanController extends Controller
{
    protected $routes = 'master.bantuan';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Bantuan' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Bantuan',
                'width' => '250px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Bantuan::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';

                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                   ]);
                   return $buttons;
               })
               ->rawColumns(['alamat', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.bantuan.index');
    }

    public function create()
    {
        return $this->render('modules.master.bantuan.create');
    }

    public function store(BantuanRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Bantuan;
	        $record->fill($request->all());
	        $record->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(BU $user)
    // {
    //     return $user->toJson();
    // }

    public function edit($data)
    {
    	$bantuan = Bantuan::find($data);
        return $this->render('modules.master.bantuan.edit', ['record' => $bantuan]);
    }

    public function update(BantuanRequest $request, Bantuan $bantuan)
    {
    	DB::beginTransaction();
        try {
        	$record = Bantuan::find($request->id);
	        $record->fill($request->all());
	        $record->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Bantuan $bantuan)
    {
        if($bantuan){
    		
    	}else{
    		return response([
                'status' => true,
            ],500);
    	}
    }
}
