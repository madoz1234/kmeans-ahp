<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Requests\Master\ClusterRequest;
use App\Models\Master\Cluster;
use App\Models\Master\ClusterDetail;
use App\Models\Master\Bantuan;
use App\Models\SessionLog;

use DB;

class ClusterController extends Controller
{
    protected $routes = 'master.cluster';

    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->setBreadcrumb(['Master' => '#', 'Cluster' => '#']);
        // Header Grid Datatable
        $this->setTableStruct([
            [
                'data' => 'num',
                'name' => 'num',
                'label' => '#',
                'orderable' => false,
                'searchable' => false,
                'className' => 'text-center',
                'width' => '20px',
            ],
            /* --------------------------- */
            [
                'data' => 'nama',
                'name' => 'nama',
                'label' => 'Nama Cluster',
                'width' => '250px',
                'sortable' => true,
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'label' => 'Dibuat Pada',
                'className' => 'text-center',
                'sortable' => true,
                'width' => '120px',
            ],
            [
                'data' => 'action',
                'name' => 'action',
                'label' => 'Aksi',
                'searchable' => false,
                'sortable' => false,
                'width' => '80px',
                'className' => 'text-center'
            ]
        ]);
    }

    public function grid()
    {
        $records = Cluster::select('*');
        if(!isset(request()->order[0]['column'])) {
              $records->orderBy('created_at');
        }
        if ($name = request()->name) {
            $records->where('nama', 'like', '%' . $name . '%');
        }
        return DataTables::of($records->get())
               ->addColumn('num', function ($record) {
                   return request()->start;
               })
               ->addColumn('nama', function ($record) {
                   return $record->nama;
               })
               ->editColumn('created_at', function ($record) {
                   return $record->created_at->diffForHumans();
               })
               ->addColumn('action', function ($record) {
                   $buttons = '';
                   $buttons .= $this->makeButtons([
                        'type'      => 'edit',
                        'label'     => '<i class="fa fa-book text-primary"></i>',
                        'tooltip'   => 'Detail',
                        'class'     => 'detil button',
                        'id'        => $record->id,
                   ]); 
                   $buttons .= '&nbsp;';
                   $buttons .= '&nbsp;';
                   $buttons .= '&nbsp;';
                   $buttons .= $this->makeButton([
                        'type' => 'edit',
                        'id'   => $record->id,
                   ]);
                   return $buttons;
               })
               ->rawColumns(['alamat', 'action'])
               ->make(true);
    }

    public function index()
    {
        return $this->render('modules.master.cluster.index');
    }

    public function create()
    {
        return $this->render('modules.master.cluster.create');
    }

    public function store(ClusterRequest $request)
    {
    	DB::beginTransaction();
    	try {
    		$record = new Cluster;
	        $record->fill($request->all());
	        $record->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    // public function show(BU $user)
    // {
    //     return $user->toJson();
    // }
    public function simpan(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->detail as $key => $value) {
                if(!empty($value['id'])){
                    $kasus_detail = ClusterDetail::find($value['id']);
                    $kasus_detail->clusterid = $request->id;
                    $kasus_detail->bantuanid = $key;
                    $kasus_detail->nilai = $value['nilai'];
                    $kasus_detail->save();
                }else{
                    $kasus_detail = new ClusterDetail;
                    $kasus_detail->clusterid = $request->id;
                    $kasus_detail->bantuanid = $key;
                    $kasus_detail->nilai = $value['nilai'];
                    $kasus_detail->save();
                }
            }
            DB::commit();
            return response([
              'status' => true
            ]); 
        }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }


    public function detail(Cluster $id)
    {
       $bantuan = Bantuan::get();
       return $this->render('modules.master.cluster.detail', [
        'record' => $id,
        'bantuan' => $bantuan,
       ]);
    }


    public function edit($data)
    {
    	$cluster = Cluster::find($data);
        return $this->render('modules.master.cluster.edit', ['record' => $cluster]);
    }

    public function update(ClusterRequest $request, Cluster $cluster)
    {
    	DB::beginTransaction();
        try {
        	$record = Cluster::find($request->id);
	        $record->fill($request->all());
	        $record->save();

	    	DB::commit();
	        return response([
	          'status' => true
	        ]); 
	    }catch (\Exception $e) {
          DB::rollback();
          return response([
            'status' => 'error',
            'message' => 'An error occurred!',
            'error' => $e->getMessage(),
          ], 500);
        }
    }

    public function destroy(Cluster $cluster)
    {
        if($cluster){
    		
    	}else{
    		return response([
                'status' => true,
            ],500);
    	}
    }
}
