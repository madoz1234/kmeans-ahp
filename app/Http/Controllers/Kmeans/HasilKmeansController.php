<?php

namespace App\Http\Controllers\Kmeans;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\CoreSn;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Master\Alternatif;
use App\Models\Master\Bantuan;
use App\Models\Master\Cluster;
use App\Models\Kmeans\NilaiAlternatif;
use App\Models\Ahp\HasilAHP;
use App\Models\Kmeans\NilaiAlternatifDetail;
use Carbon\Carbon;

class HasilKmeansController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $routes = 'kmeans.hasil';
    public function __construct()
    {
        $this->setRoutes($this->routes);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $nilai = NilaiAlternatif::get();
        $bantuan = Bantuan::get();
        $alternatif = Alternatif::get();
        $nilaidetail = NilaiAlternatifDetail::get();
        $ahp = HasilAHP::get();
        $cluster = Cluster::get();
         foreach($ahp as $ahps){
            foreach($nilai->where('alternatif_id', $ahps->alternatif->id) as $alternatifs){
              foreach($bantuan as $bantuans){
                $nilai_detail = $alternatifs->detail->where('bantuan_id', $bantuans->id)->first();
              }
            }
         }
        return $this->render('modules.kmeans.hasil', [
            'mockup' => true,
            'bantuan' => $bantuan,
            'alternatif' => $alternatif,
            'nilai' => $nilai,
            'nilaidetail' => $nilaidetail,
            'cluster' => $cluster,
            'ahp' => $ahp,
        ]);
    }
}
