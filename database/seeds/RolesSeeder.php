<?php

use App\Models\Auths\Role;
use App\Models\Auths\User;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin 		         	= Role::create(['name' => 'admin']);

        $admin->users()->save(new User([
            'name'  => 'Super Admin',
            'email' => 'admin@email.com',
            'phone' => '+62 999 9991',
            'password' => bcrypt('password'),
        ]));
    }
}
