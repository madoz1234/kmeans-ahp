<?php

use Illuminate\Database\Seeder;

use App\Models\Auths\User;
use Spatie\Permission\Models\Role;


class UserSeeder extends Seeder
{
    public function run()
    {

    	// create user
		$user = [
			[
				'name'  => 'Super Admin',
				'email' => 'admin@email.com',
				'phone' => '+62 999 9991',
				'password' => bcrypt('password'),
				'perms' => 'admin',
				'fungsi' => '0',
			]
		];
		
		foreach($user as $data){
			$admin = new User();
			$admin->name   = $data['name'];
			$admin->password   = $data['password'];
			$admin->email   = $data['email'];
			$admin->phone = $data['phone'];
			$admin->save();
			$role_admin = Role::findByName($data['perms']);
			$admin->roles()->sync([$role_admin->id]);
		}
    }
}
