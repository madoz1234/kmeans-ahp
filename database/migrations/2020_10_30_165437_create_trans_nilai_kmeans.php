<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransNilaiKmeans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans_nilai_kmeans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alternatifid')->unsigned()->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->nullableTimestamps();

            $table->foreign('alternatifid')->references('id')->on('ref_alternatif');
        });

        Schema::create('trans_nilai_kmeans_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kmeansid')->unsigned()->default(1);
            $table->integer('cluster_detail_id')->unsigned()->default(1);
            $table->integer('nilai');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('kmeansid')->references('id')->on('trans_nilai_kmeans');
            $table->foreign('cluster_detail_id')->references('id')->on('ref_cluster_detail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans_nilai_kmeans_detail');
        Schema::dropIfExists('trans_nilai_kmeans');
    }
}
