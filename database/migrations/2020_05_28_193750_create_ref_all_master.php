<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefAllMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref_bantuan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 200);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_kriteria', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 200)->nullable();
            $table->string('nama', 200);
            $table->integer('bobot');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_alternatif', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 200);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_kriteria', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('kode', 200)->nullable();
            $table->string('nama', 200);
            $table->integer('bobot');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('log_ref_alternatif', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('nama', 200);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_cluster', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 200);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();
        });

        Schema::create('ref_cluster_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clusterid')->unsigned()->default(1);
            $table->integer('bantuanid')->unsigned()->default(1);
            $table->float('nilai', 5, 3);
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->nullableTimestamps();

            $table->foreign('clusterid')->references('id')->on('ref_cluster');
            $table->foreign('bantuanid')->references('id')->on('ref_bantuan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('log_ref_alternatif');
    	Schema::dropIfExists('log_ref_kriteria');
    	Schema::dropIfExists('ref_alternatif');
        Schema::dropIfExists('ref_kriteria');
        Schema::dropIfExists('ref_bantuan');
        Schema::dropIfExists('ref_cluster_detail');
        Schema::dropIfExists('ref_cluster');
    }
}
